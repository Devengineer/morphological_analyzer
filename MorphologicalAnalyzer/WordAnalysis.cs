﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MorphologicalAnalyzer
{

    /// <summary>
    /// WordAnalysis - основной класс программы, который
    /// добавляет слова в словарь
    /// определяет морфологические характеристики введённых слов
    /// склоняет слово согласно введённым морфологическим характеристикам
    /// </summary>
    public sealed class WordAnalysis
    {
        //Список букв, которые есть в словах указанных в словаре
        List<LetterData> letters = new List<LetterData>();
        //Список окнчаний без неизменяемой части
        List<OnlySuffixData> onlySuffix = new List<OnlySuffixData>();
        //Хеш таблица неизменяемых частей
        Hashtable suffixTable = new Hashtable();


        #region Конструтор класса
        //В конструкторе загружаем данные из файлов
        private WordAnalysis()
        {
            //Загрузка окончаний в хеш таблицу
            StreamReader endData = new StreamReader(@"flexia.txt", Encoding.Default);
            string str;
            while (!endData.EndOfStream)
            {
                str = endData.ReadLine();
                suffixTable.Add(Int32.Parse(Regex.Match(str, @"[0-9]*").Value), Regex.Replace(str, @"^[0-9]|:|\s", ""));
            }
            endData.Close();

            //Загрузка неизменяемых частей
            StreamReader basisData = new StreamReader(@"word.txt", Encoding.Default);
            string[] wordDict;
            while (!basisData.EndOfStream)
            {
                str = basisData.ReadLine();
                wordDict = str.Split(':');
                if (wordDict.Count() == 3)
                {
                    AddWord(wordDict[0].Trim(' '), wordDict[1].Trim(' '), Convert.ToInt32(wordDict[2].Trim(' ')));
                }
                AddWord(wordDict[0].Trim(' '), wordDict[1].Trim(' '));
            }
            basisData.Close();
        }
        #endregion

        //Инициализируем единственный объект класса
        private static WordAnalysis wordAnalysis = new WordAnalysis();

        //Метод для получениядоступа к экземпляру класса
        public static WordAnalysis GetInstance()
        {
            return wordAnalysis;
        }

        #region Добавление слова
        /// <summary>
        /// Функция предназанчена для добавления слова в словарь, который находится в оперативной памяти
        /// </summary>
        /// <param name="_word">Неизменяемая часть слова</param>
        /// <param name="_morphologData">Морфологические описания из файла words.txt</param>
        /// <param name="idSuffix">id неизменямой части слова из файла flexia.txt, если нет неизменямой части, то значение -1</param>
        private void AddWord(string _word, string _morphologData, int idSuffix = -1)
        {
            if (_word.Length == 0)
            {
                this.onlySuffix.Add(new OnlySuffixData(_morphologData,  this.suffixTable, idSuffix));
            }
            else
            {
                for (int i = 0; i < _word.Length; i++)
                {
                    if (i == _word.Length - 1)
                    {
                        //Если буква неизменямой части последняя, тогда берём описание из первого словаря
                        this.letters.Add(new LetterData(_word[i], i, _morphologData, true, _word, this.suffixTable, idSuffix));
                    }
                    else
                    {
                        this.letters.Add(new LetterData(_word[i], i));
                    }
                }
            }
        }
        #endregion

        #region Поиск слова
        //Поиск в словаре по входной строке
        /// <summary>
        /// Осуществляет поиск слова по заданным параметрам
        /// </summary>
        /// <param name="_searchWord">Введённое пользователем слово</param>
        /// <param name="_changeFrom">Атрибут указывает на режим работы
        /// true - склоняет слово
        /// false - находит морфологические признаки по каждому слову</param>
        /// <param name="_morphologInput">Желаемые морфлогические характеристики для склоняемого слова</param>
        /// <returns>Возвращает либо строку с морфологическими характеристиками, 
        /// либо слово с заданными характеристиками</returns>
        private string FindWord(string _searchWord, bool _changeFrom, string _morphologInput = "")
        {
            string resultAnalysis = "";
            string suffixData;
            for (int i = 0; i < _searchWord.Length; i++)
            {
                //Если есть хоть одна буква с текущей позицией
                var resLetter = letters.Where(let => let.letter == _searchWord[i] && let.position == i);
                if (resLetter.Any())
                {
                    //Перебираем все найденные буквы 
                    foreach (LetterData let in resLetter)
                    {
                        //Являтеся ли буква последней для неизменяемой части слова
                        if (let.last)
                        {
                            suffixData = let.GetSuffixData(_searchWord.Substring(i + 1), _searchWord.Substring(0, i + 1));
                            if (suffixData != "")
                            {
                                if (_changeFrom)
                                {
                                    string sklonenie = Regex.Match(this.suffixTable[let.idSuffix].ToString(), @"(\w+|)," + _morphologInput).Value;
                                    if (sklonenie != "")
                                    {
                                        return _searchWord.Substring(0, i + 1) + sklonenie.Split(',')[0];
                                    }
                                    else
                                    {
                                        return "Ошибка ввода морфологических характеристик";
                                    }
                                }
                                string beginForm = Regex.Match(suffixTable[let.idSuffix].ToString(), @"^\w+").Value;
                                return resultAnalysis = "начальная форма \"" + _searchWord.Substring(0, i + 1) + beginForm + "\"," + let.morphologData + ',' + suffixData;
                            }
                            //Если букв не осталось и слово не имеет неизменяемой части, 
                            //то возвращаем морфологическое описание из первого документа
                            if (let.idSuffix == -1 && _searchWord.Substring(i + 1) == "")
                            {
                                if (_changeFrom) return "";
                                return let.morphologData;
                            }
                        }
                    }
                }
                else
                {
                    string onlySufMorpholog;
                    foreach (OnlySuffixData os in onlySuffix)
                    {
                        onlySufMorpholog = os.GetSuffixData(_searchWord);
                        if (onlySufMorpholog != "")
                        {
                            if (_changeFrom)
                            {
                                string sklonenie = Regex.Match(this.suffixTable[os.idSuffix].ToString(), @"(\w+|)," + _morphologInput).Value;
                                if (sklonenie != "")
                                {
                                    return sklonenie.Split(',')[0];
                                }
                                else
                                {
                                    return "Ошибка ввода морфологических характеристик";
                                }


                            }
                            string beginForm = Regex.Match(this.suffixTable[os.idSuffix].ToString(), @"^\w+").Value;
                            return resultAnalysis = "начальная форма \"" + beginForm + "\"," + os.morphologData + ',' + onlySufMorpholog;
                        }
                    }
                    //Сбросить состояние поиска слова, т.к. слово не найдено
                    resultAnalysis = "";
                    break;
                }
            }
            return resultAnalysis;
        }
        #endregion

        #region Вывод результата
        /// <summary>
        /// Принмиает входные данные по режиму работы программы и выводит результаты работы
        /// </summary>
        /// <param name="_inputStr">Входная строка для обработки</param>
        /// <param name="_changeForm">Выбрать сценраий работы программы
        /// true - изменить слово по заданным морфологическим признакам
        /// false - получить морфлогические данные по каждому введённому слову</param>
        /// <param name="_morphologInput">Желаемые морфологические характиристики для склоняемого слова</param>
        /// <returns>Возвращает строку с морфологическими характеристиками по каждому слову или изменённое слово 
        /// согласно морфологичесикм харатеристикам</returns>
        public string StringAnalysis(string _inputStr, bool _changeForm, string _morphologInput = "")
        {
            string strResult = "";
            if (_changeForm)
            {
                strResult = FindWord(_inputStr.ToLower(), true, _morphologInput);
                if (strResult == "")
                {
                    strResult = _inputStr + " - слово не найдено\n";
                }
            }
            else
            {
                string wordFinded;
                MatchCollection allWords = Regex.Matches(_inputStr.ToLower(), "[а-яА-Я]+");
                //Поочерёдно для каждого слова выполняем морфологический анализ
                foreach (Match wordInput in allWords)
                {
                    wordFinded = FindWord(wordInput.Value, false);
                    if (wordFinded != "")
                    {
                        strResult += wordInput + " - " + wordFinded + "\n";

                    }
                    else
                    {
                        strResult += wordInput + " - слово не найдено\n";
                    }
                }
            }
            return strResult;
        }
        #endregion
    }

    /// <summary>
    /// Класс для хранения изменяемой части слова
    /// </summary>
    class Suffix
    {
        //Неизменяемая часть слова
        public string basis;
        //Таблица окончаний
        public Hashtable suffix = new Hashtable();

        #region Конструктор класса Suffix
        /// <summary>
        /// Конструктор класса создаёт объект содержащий основу слова и соответствующие ей 
        /// возможные варианты неизменяемой части слова с морфологическими характеристиками
        /// </summary>
        /// <param name="_basis">Неизменяемая часть слова</param>
        /// <param name="_suffix">Строка содержащая все варианты окончаний для данного слова</param>
        public Suffix(string _basis, string _suffix)
        {
            basis = _basis;
            string[] suffixData = _suffix.Split(';');
            string suffixKey, suffixVal;


            foreach (string sd in suffixData)
            {
                suffixKey = Regex.Match(sd, @"[а-яА-Я]*").Value;
                suffixVal = Regex.Replace(sd, @"^[а-яА-Я]*,", "");
                //Заполняем таблицу окончаний для добавленного  слова
                if (!suffix.ContainsKey(suffixKey))
                {
                    suffix.Add(Regex.Match(sd, @"[а-яА-Я]*").Value, suffixVal);
                }
                else
                {
                    suffix[suffixKey] += " или " + suffixVal;
                }

            }
        }
        #endregion
    }

    //Авбстрактный класс содержащий общую информацию для хранимых символов
    abstract class CharData
    {   //Морфологические данные неизменяемой части слова
        public string morphologData;
        //id неизменяемой части из файла flexia
        public int idSuffix;
        public List<Suffix> suffixes = new List<Suffix>();
        public abstract string GetSuffixData(string _endWord, string _basis = "");
    }

    //Класс содержащий данные о словах у которых нет неизменяемой части
    class OnlySuffixData : CharData
    {
        /// <summary>
        /// Конструктор класса создаёт объект в котором хранится описание из файла words и соответствующей части
        /// из файла flexia
        /// </summary>
        /// <param name="_morphologData">морфологические признаки из файла words.txt</param>
        /// <param name="_suffixTable">Хеш-таблица с данными по неизменяемой части</param>
        /// <param name="_idSuffix">id необходмой нам изменяемой части</param>
        public OnlySuffixData(string _morphologData,  Hashtable _suffixTable, int _idSuffix = -1)
        {
            morphologData = _morphologData;
            idSuffix = _idSuffix;
            if (_suffixTable[_idSuffix] != null)
            {
                suffixes.Add(new Suffix("", _suffixTable[_idSuffix].ToString()));
            }
        }
        //Получить морфологический анализ для слова без неизменяемой части
        public override string GetSuffixData(string _endWord, string _basis = "")
        {
            var resSuffix = this.suffixes.SingleOrDefault(suf => suf.suffix.ContainsKey(_endWord));
            if (resSuffix != null)
            {
                return resSuffix.suffix[_endWord].ToString();
            }
            else
            {
                return "";
            }
        }
    }

    //Данные о буквах в словах неизменяемой части
    class LetterData : CharData
    {
        //Является ли буква конечной для какого либо из добавленных слов
        public bool last;
        //Позиция буквы в добавляемом слове
        public int position;
        //Буква слова
        public char letter;
        /// <summary>
        /// Конструктор создаёт объект содержащий данные по букве
        /// </summary>
        /// <param name="_letter">Буква</param>
        /// <param name="_position">Позиция буквы в слове</param>
        /// <param name="_morphologData">Морфологические признаки неизменяемой части слова</param>
        /// <param name="_last">Является ли буква последней в неизменяемой части</param>
        /// <param name="_basis">Неизменяемая часть слова</param>
        /// <param name="_suffixTable">Таблица окончаний</param>
        /// <param name="_idSuffix">id изменяемой части слова</param>
        public LetterData(char _letter, int _position, string _morphologData = "", bool _last = false, string _basis = "", Hashtable _suffixTable = null, int _idSuffix = -1)
        {

            letter = _letter;
            position = _position;
            morphologData = _morphologData;
            last = _last;
            idSuffix = _idSuffix;
            if (idSuffix != -1 && _suffixTable[_idSuffix] != null)
            {
                this.suffixes.Add(new Suffix(_basis, _suffixTable[_idSuffix].ToString()));
            }
        }
        //Получить морфологический анализ изменяемой части слова
        public override string GetSuffixData(string _endWord, string _basis)
        {
            var resSuffix = this.suffixes.SingleOrDefault(suf => suf.basis == _basis && suf.suffix.ContainsKey(_endWord));
            if (resSuffix != null)
            {
                return resSuffix.suffix[_endWord].ToString();
            }
            else
            {
                return "";
            }
        }
    }
}
